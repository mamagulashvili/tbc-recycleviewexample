package com.example.tbcrecycleexample

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcrecycleexample.databinding.RowItemBinding

class Adapter(private val contentList: MutableList<BookModel>, private val context: Context) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    inner class ViewHolder(val binding: RowItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            val item = contentList[adapterPosition]
            binding.apply {
                titleTextView.text = item.title
                authorTextView.text = item.author
                releaseDateTextView.text = item.releaseDate
            }
            itemView.apply {
                Glide.with(this).load(item.imageUrl).into(binding.rowImageView)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
        val item = contentList[position]
        holder.itemView.setOnClickListener {
            showDialog(item.title,item.author)
        }
    }

    override fun getItemCount() = contentList.size

    private fun showDialog(title: String, author: String) {
        val builder = AlertDialog.Builder(context).apply {
            setTitle(title)
            setIcon(R.drawable.ic_book)
            setMessage("This Is  $author's book")
            setPositiveButton("Ok") { _, _ -> }
        }.create()
        builder.show()

    }
}