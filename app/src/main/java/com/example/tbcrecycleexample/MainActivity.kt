package com.example.tbcrecycleexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcrecycleexample.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val contentList = mutableListOf<BookModel>()
    private val recycleAdapter: Adapter by lazy { Adapter(contentList, this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setRecycleView()
        setData()
    }

    private fun setRecycleView() {
        binding.recycleView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = recycleAdapter
        }
    }

    private fun setData() {
        contentList.apply {
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/sorcerors-stone.jpg",
                    "Harry Potter - Sorcerers Stone",
                    "J. K. Rowling",
                    "1997 June 27"
                )
            )
            add(
                BookModel(
                    "https://images-na.ssl-images-amazon.com/images/I/71MUiF4iVzL._AC_SY606_.jpg",
                    "Harry Potter and the Chamber of Secrets",
                    "J. K. Rowling",
                    "1998 2 Jule"
                )
            )
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/azkaban.jpeg",
                    "Harry Potter and the Prisoner of Azkaban ",
                    "J. K. Rowling",
                    "1999 8 Jule"
                )
            )
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/HP4cover.jpg",
                    "Harry Potter and the Goblet of Fire",
                    "J. K. Rowling",
                    "2000 Jule 8"
                )
            )
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/order-of-phoenix.jpg",
                    "Harry Potter and the Order of the Phoenix",
                    "J. K. Rowling",
                    "2003 21 June"
                )
            )
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/use-this-hp-signature-.jpg",
                    "Harry Potter and the Half-Blood Prince",
                    "J. K. Rowling",
                    "2005 16 Jule"
                )
            )
            add(
                BookModel(
                    "https://universe.byu.edu/wp-content/uploads/2015/01/Hp71.jpg",
                    "Harry Potter and the Deathly Hallows",
                    "J. K. Rowling",
                    "2007 21 Jule"
                )
            )
            add(
                BookModel(
                    "https://cdn.hmv.com/r/w-1280/hmv/files/27/2759b205-54a1-4bc0-a753-e8ce5c4754b6.jpg",
                    "The Headless Horseman",
                    "Mayne Reid",
                    "1865–1866"
                )
            )
            add(
                BookModel(
                    "https://images.penguinrandomhouse.com/cover/9780141329390",
                    "The Hound of the Baskervilles",
                    "Arthur Conan Doyle",
                    "1902"
                )
            )
        }
        recycleAdapter.notifyDataSetChanged()
        itemSwipeDelete()
    }

    private fun itemSwipeDelete() {
        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val item = contentList[position]
                contentList.remove(item)
                recycleAdapter.notifyItemRemoved(position)
                Snackbar.make(binding.mainContainer, "Successfully Deleted", Snackbar.LENGTH_SHORT)
                    .apply {
                        setAction("Undo") {
                            contentList.add(item)
                            recycleAdapter.notifyDataSetChanged()
                        }.show()
                    }
            }

        }
        ItemTouchHelper(itemTouchHelper).apply {
            attachToRecyclerView(binding.recycleView)
        }
    }


}