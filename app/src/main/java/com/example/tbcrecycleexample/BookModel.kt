package com.example.tbcrecycleexample

data class BookModel(
    val imageUrl:String,
    val title:String,
    val author:String,
    val releaseDate:String
)
